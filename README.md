# Go rpc

This is a simple example of how to use rpc in go.
```shell
docker-compose up
```

Links to other services:
```
https://gitlab.com/Vallyenfail/grpc-user
https://gitlab.com/Vallyenfail/grpc-crypto
https://gitlab.com/Vallyenfail/grpc-auth
```

