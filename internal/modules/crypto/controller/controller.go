package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Vallyenfail/grpc-gateway/internal/infrastructure/component"
	"gitlab.com/Vallyenfail/grpc-gateway/internal/infrastructure/errors"
	"gitlab.com/Vallyenfail/grpc-gateway/internal/infrastructure/handlers"
	"gitlab.com/Vallyenfail/grpc-gateway/internal/infrastructure/responder"
	"gitlab.com/Vallyenfail/grpc-gateway/internal/modules/crypto/service"
	"net/http"
)

type Crypter interface {
	Cost(w http.ResponseWriter, r *http.Request)
	History(w http.ResponseWriter, r *http.Request)
	Max(w http.ResponseWriter, r *http.Request)
	Min(w http.ResponseWriter, r *http.Request)
	Avg(w http.ResponseWriter, r *http.Request)
}

type Crypto struct {
	crypto service.Crypter
	responder.Responder
	godecoder.Decoder
}

func NewCrypto(crypto service.Crypter, components *component.Components) *Crypto {
	return &Crypto{
		crypto:    crypto,
		Responder: components.Responder,
		Decoder:   components.Decoder,
	}
}

func (c *Crypto) Cost(w http.ResponseWriter, r *http.Request) {
	_, err := handlers.ExtractUser(r)
	if err != nil {
		c.ErrorBadRequest(w, err)
		return
	}
	out := c.crypto.Cost(r.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(w, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(w, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (c *Crypto) History(w http.ResponseWriter, r *http.Request) {
	_, err := handlers.ExtractUser(r)
	if err != nil {
		c.ErrorBadRequest(w, err)
		return
	}
	out := c.crypto.History(r.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(w, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data:      Data{Message: "retrieving user error"},
		})
		return
	}

	c.OutputJSON(w, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data:      Data{Crypto: out.Coins},
	})
}

func (c *Crypto) Max(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.Max(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (c *Crypto) Min(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.Min(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (c *Crypto) Avg(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.Avg(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}
