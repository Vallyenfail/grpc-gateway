package models

import "encoding/json"

type Coins map[string]CoinsValue

func UnmarshalPairs(data []byte) (Coins, error) {
	var r Coins
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Coins) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type CoinsValue struct {
	BuyPrice  string `json:"buy_price"`
	SellPrice string `json:"sell_price"`
	LastTrade string `json:"last_trade"`
	High      string `json:"high"`
	Low       string `json:"low"`
	Avg       string `json:"avg"`
	Vol       string `json:"vol"`
	VolCurr   string `json:"vol_curr"`
	Updated   int64  `json:"updated"`
}

type Coin struct {
	ID        int64   `json:"id,omitempty"`
	Name      string  `json:"name,omitempty"`
	BuyPrice  string  `json:"buy_price,omitempty"`
	SellPrice string  `json:"sell_price,omitempty"`
	LastTrade string  `json:"last_trade,omitempty"`
	High      float64 `json:"high,string,omitempty"`
	Low       float64 `json:"low,string,omitempty"`
	Avg       string  `json:"avg,omitempty"`
	Vol       string  `json:"vol,omitempty"`
	VolCurr   string  `json:"vol_curr,omitempty"`
	Updated   int64   `json:"updated,omitempty"`
}
